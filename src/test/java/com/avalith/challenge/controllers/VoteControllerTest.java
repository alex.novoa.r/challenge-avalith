package com.avalith.challenge.controllers;

import com.avalith.challenge.dtos.VoteEmployeeDto;
import com.avalith.challenge.security.JwtTokenUtil;
import com.avalith.challenge.security.JwtUserDetailsServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import java.util.Date;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class VoteControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private JwtUserDetailsServiceImpl userDetailsService;

    @Test
    @WithMockUser(username = "alex",roles = "USER")
    public void testVote() throws Exception {
        UserDetails userDetails = this.userDetailsService.loadUserByUsername("alex");
        String token = this.jwtTokenUtil.obtainToken(userDetails);
        mvc.perform(post("/api/v1/votes")
                .contentType("application/json")
                .header("authorization"," "+token)
                .content(this.jsonVote()))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(username = "alex",roles = "USER")
    public void testVoteSameUser() throws Exception {
        UserDetails userDetails = this.userDetailsService.loadUserByUsername("alex");
        String token = this.jwtTokenUtil.obtainToken(userDetails);
        mvc.perform(post("/api/v1/votes")
                .contentType("application/json")
                .header("authorization"," "+token)
                .content(this.jsonVoteSameUser()))
                .andExpect(status().isBadRequest());
    }

    private String jsonVote() throws JsonProcessingException {
        VoteEmployeeDto voteEmployeeDto = new VoteEmployeeDto();
        voteEmployeeDto.setEmployee(2);
        voteEmployeeDto.setVoteDate(new Date());
        voteEmployeeDto.setArea(1);
        voteEmployeeDto.setComment("Comment");
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(voteEmployeeDto);
    }

    private String jsonVoteSameUser() throws JsonProcessingException {
        VoteEmployeeDto voteEmployeeDto = new VoteEmployeeDto();
        voteEmployeeDto.setEmployee(1);
        voteEmployeeDto.setVoteDate(new Date());
        voteEmployeeDto.setArea(1);
        voteEmployeeDto.setComment("Comment");
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(voteEmployeeDto);
    }
}
