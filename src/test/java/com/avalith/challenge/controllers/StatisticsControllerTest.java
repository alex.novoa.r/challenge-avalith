package com.avalith.challenge.controllers;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class StatisticsControllerTest {

    @Autowired
    private MockMvc mvc;

    @Test
    @WithMockUser(username = "alex", roles = "ADMIN")
    public void testTotalEmployee() throws Exception {
        mvc.perform(get("/api/v1/statistics/total" )).andExpect(status().isOk())
                .andExpect(jsonPath("$.total").value("3" ));
    }

}
