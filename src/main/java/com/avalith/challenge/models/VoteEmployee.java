package com.avalith.challenge.models;

import com.avalith.challenge.utils.JsonDateSerializer;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
public class VoteEmployee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @ManyToOne
    @JoinColumn(name = "employee")
    private Employee employee;
    @ManyToOne
    @JoinColumn(name = "applicationUser")
    @JsonIgnore
    private AplicationUser aplicationUser;
    @ManyToOne
    @JoinColumn(name = "area")
    private Area area;
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "America/Guayaquil")
    @JsonSerialize(using = JsonDateSerializer.class)
    private Date voteDate;
    private String comment;
    private int month;
}
