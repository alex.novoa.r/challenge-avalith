package com.avalith.challenge.models;

import lombok.Getter;
import lombok.Setter;
import javax.persistence.*;

@Entity
@Getter
@Setter
public class AplicationUser {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(length = 10)
    private String username;
    private String role;
    private String password;

    public AplicationUser() {
    }

    public AplicationUser(String username, String password, String role) {
        this.username= username;
        this.password=password;
        this.role=role;
    }
}
