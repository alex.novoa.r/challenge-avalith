package com.avalith.challenge.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(length = 25)
    private String lastname;
    @Column(length = 25)
    private String name;
    @ManyToOne
    @JoinColumn(name = "aplicationUser")
    @JsonIgnore
    private AplicationUser aplicationUser;
}
