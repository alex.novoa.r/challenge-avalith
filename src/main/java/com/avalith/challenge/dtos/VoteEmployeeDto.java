package com.avalith.challenge.dtos;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import java.util.Date;

@Getter
@Setter
@ToString
public class VoteEmployeeDto {
    private long id;
    private long employee;
    private long area;
    private Date voteDate;
    private String comment;

    public VoteEmployeeDto() {
    }

    public VoteEmployeeDto(long id, long employee, long area, Date voteDate, String comment) {
        this.id = id;
        this.employee = employee;
        this.area = area;
        this.voteDate = voteDate;
        this.comment = comment;
    }
}
