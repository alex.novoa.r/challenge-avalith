package com.avalith.challenge.dtos;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class EmployeeTotal {

    private int total;

    public EmployeeTotal(int total) {
        this.total=total;
    }


}
