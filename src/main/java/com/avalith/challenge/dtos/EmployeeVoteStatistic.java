package com.avalith.challenge.dtos;

import com.avalith.challenge.models.Employee;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class EmployeeVoteStatistic {

    private Employee employee;
    private long count;

    public EmployeeVoteStatistic(Employee employee, long count ) {
        this.count = count;
        this.employee = employee;
    }
}
