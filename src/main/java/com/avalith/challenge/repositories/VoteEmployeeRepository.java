package com.avalith.challenge.repositories;

import com.avalith.challenge.dtos.EmployeeVoteStatistic;
import com.avalith.challenge.models.AplicationUser;
import com.avalith.challenge.models.Area;
import com.avalith.challenge.models.VoteEmployee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import java.util.List;

public interface VoteEmployeeRepository extends JpaRepository<VoteEmployee, Long> {

    List<VoteEmployee> findByAplicationUserAndMonth(AplicationUser aplicationUser, int month);

    List<VoteEmployee> findByAplicationUserAndAreaAndMonth(AplicationUser aplication, Area area, int month);

    @Query("SELECT new com.avalith.challenge.dtos.EmployeeVoteStatistic(e.employee, count (e)) "
            + " FROM VoteEmployee e "
            + " WHERE e.month = :month"
            + " GROUP BY e.employee"
            + " ORDER BY COUNT(e) DESC")
    List<EmployeeVoteStatistic> getCountEmployee(int month);

    @Query("SELECT new com.avalith.challenge.dtos.EmployeeVoteStatistic(e.employee, count (e)) "
            + " FROM VoteEmployee e "
            + " WHERE e.area = :area "
            + " GROUP BY e.employee"
            + " ORDER BY COUNT(e) DESC")
    List<EmployeeVoteStatistic> findByArea(Area area);

}
