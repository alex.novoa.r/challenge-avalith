package com.avalith.challenge.repositories;

import com.avalith.challenge.models.Area;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AreaRepository extends JpaRepository<Area, Long> {

}
