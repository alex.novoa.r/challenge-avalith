package com.avalith.challenge.repositories;

import com.avalith.challenge.models.AplicationUser;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AplicationUserRepository extends JpaRepository<AplicationUser, Long> {

    AplicationUser findByUsername(String username);
}
