package com.avalith.challenge.repositories;

import com.avalith.challenge.models.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import java.util.List;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {

    @Query("SELECT e FROM Employee e WHERE e.aplicationUser.username <> ?1")
    List<Employee> getAll(String username);

    @Query("SELECT count (e) FROM Employee  e")
    int getCountAll();
}
