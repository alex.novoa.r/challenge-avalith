package com.avalith.challenge.controllers;

import com.avalith.challenge.dtos.EmployeeVoteStatistic;
import com.avalith.challenge.dtos.EmployeeTotal;
import com.avalith.challenge.models.Area;
import com.avalith.challenge.services.AreaService;
import com.avalith.challenge.services.EmployeeService;
import com.avalith.challenge.services.VoteEmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
@RequestMapping("/api")
public class StatisticsController {

    @Autowired
    private EmployeeService employeeService;
    @Autowired
    private VoteEmployeeService voteEmployeeService;
    @Autowired
    private AreaService areaService;


    @GetMapping("/v1/statistics/total")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<EmployeeTotal> getTotalEmployee(){
        int total = employeeService.getCountAll();
        EmployeeTotal employeeTotal = new EmployeeTotal(total);
        return ResponseEntity.ok(employeeTotal);
    }

    @GetMapping("/v1/statistics")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<List<EmployeeVoteStatistic>> getMoreVoted(@RequestParam int month){
        List<EmployeeVoteStatistic> employeeVoteStatistics = new ArrayList<>();
        employeeVoteStatistics = voteEmployeeService.getCountEmployee(month);
        return ResponseEntity.ok(employeeVoteStatistics);
    }

    @GetMapping("/v1/statistics/area")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<List<EmployeeVoteStatistic>> getVotedByArea(@RequestParam long idArea){
        Area area = (Area) areaService.findById(idArea).get();
        List<EmployeeVoteStatistic> voteEmployees = voteEmployeeService.findByArea(area);
        return ResponseEntity.ok(voteEmployees);
    }
}
