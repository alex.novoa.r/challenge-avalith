package com.avalith.challenge.controllers;

import com.avalith.challenge.models.Employee;
import com.avalith.challenge.services.EmployeeService;
import com.avalith.challenge.utils.CoreUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/api")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;
    @Autowired
    private CoreUtils coreUtils;

    @GetMapping("/v1/employees")
    public ResponseEntity<List<Employee>> get(HttpServletRequest request){
        String username = coreUtils.getUserFromRequest(request);
        List<Employee> employees = employeeService.getAllExcept(username);
        return ResponseEntity.ok(employees);
    }
}
