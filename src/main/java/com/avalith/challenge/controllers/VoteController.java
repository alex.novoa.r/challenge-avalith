package com.avalith.challenge.controllers;

import com.avalith.challenge.dtos.VoteEmployeeDto;
import com.avalith.challenge.models.AplicationUser;
import com.avalith.challenge.models.Area;
import com.avalith.challenge.models.Employee;
import com.avalith.challenge.models.VoteEmployee;
import com.avalith.challenge.services.AplicationUserService;
import com.avalith.challenge.services.AreaService;
import com.avalith.challenge.services.VoteEmployeeService;
import com.avalith.challenge.utils.CoreUtils;
import com.avalith.challenge.utils.CurrentMonth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

@RestController
@RequestMapping("/api")
public class VoteController {

    @Autowired
    private VoteEmployeeService voteEmployeeService;
    @Autowired
    private CoreUtils coreUtils;
    @Autowired
    private AplicationUserService aplicationUserService;
    @Autowired
    private AreaService areaService;
    @Autowired
    private CurrentMonth currentMonth;

    @PostMapping("/v1/votes")
    public ResponseEntity<?> post(HttpServletRequest request, @RequestBody VoteEmployeeDto voteEmployeeDto){
        String username = coreUtils.getUserFromRequest(request);
        AplicationUser aplicationUser = aplicationUserService.findByUsername(username);
        Area area = (Area) areaService.findById(voteEmployeeDto.getArea()).get();
        List<VoteEmployee> voteEmployees = voteEmployeeService.findByAplicationUserAndAreaAndMonth(aplicationUser, area, currentMonth.gerCurrentMonth());
        if(!voteEmployees.isEmpty()){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body("This area has been voted in month "+currentMonth.gerCurrentMonth());
        }

        if(aplicationUser.getId()==voteEmployeeDto.getEmployee()){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body("Vote not permitted, vote yourself");
        }
        VoteEmployee voteEmployee = dtoToEntity(voteEmployeeDto);
        voteEmployee.setAplicationUser(aplicationUser);
        VoteEmployee voteEmployeeSaved = voteEmployeeService.save(voteEmployee);
        return ResponseEntity.ok(entityToDto(voteEmployeeSaved));
    }


    private VoteEmployeeDto entityToDto(VoteEmployee voteEmployee){
        VoteEmployeeDto dto = new VoteEmployeeDto();
        dto.setId(voteEmployee.getId());
        dto.setArea(voteEmployee.getArea().getId());
        dto.setComment(voteEmployee.getComment());
        dto.setEmployee(voteEmployee.getEmployee().getId());
        dto.setVoteDate(voteEmployee.getVoteDate());
        return dto;
    }

    private VoteEmployee dtoToEntity(VoteEmployeeDto voteEmployeeDto){
        VoteEmployee voteEmployee = new VoteEmployee();
        Area area = new Area();
        area.setId(voteEmployeeDto.getArea());
        voteEmployee.setArea(area);
        voteEmployee.setComment(voteEmployeeDto.getComment());
        Employee employee = new Employee();
        employee.setId(voteEmployeeDto.getEmployee());
        voteEmployee.setEmployee(employee);
        voteEmployee.setVoteDate(voteEmployeeDto.getVoteDate());
        LocalDate localDate = voteEmployeeDto.getVoteDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        voteEmployee.setMonth(localDate.getMonthValue());
        return voteEmployee;
    }
}
