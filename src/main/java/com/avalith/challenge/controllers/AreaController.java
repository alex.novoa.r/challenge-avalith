package com.avalith.challenge.controllers;

import com.avalith.challenge.models.AplicationUser;
import com.avalith.challenge.models.Area;
import com.avalith.challenge.models.VoteEmployee;
import com.avalith.challenge.services.AplicationUserService;
import com.avalith.challenge.services.AreaService;
import com.avalith.challenge.services.VoteEmployeeService;
import com.avalith.challenge.utils.CoreUtils;
import com.avalith.challenge.utils.CurrentMonth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
public class AreaController {

    @Autowired
    private AreaService areaService;
    @Autowired
    private VoteEmployeeService voteEmployeeService;
    @Autowired
    private CoreUtils coreUtils;
    @Autowired
    private AplicationUserService aplicationUserService;
    @Autowired
    private CurrentMonth currentMonth;

    /**
     *
     * @param request
     * @return
     */
    @GetMapping("/v1/areas")
    public ResponseEntity<List<Area>> get(HttpServletRequest request){
        System.out.println(currentMonth.gerCurrentMonth());
        String username = coreUtils.getUserFromRequest(request);
        AplicationUser aplicationUser = aplicationUserService.findByUsername(username);
        List<Area> areas = areaService.findAll();
        List<VoteEmployee> voteEmployees = voteEmployeeService.findByAplicationUserAndMonth(aplicationUser, currentMonth.gerCurrentMonth());
        List<Area> areasVoted = voteEmployees.stream().map(temp -> {
            Area obj= temp.getArea();
            return obj;
        }).collect(Collectors.toList());
        areas.removeAll(areasVoted);
        return ResponseEntity.ok(areas);
    }

}
