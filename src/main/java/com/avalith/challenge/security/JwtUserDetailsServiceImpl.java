package com.avalith.challenge.security;

import com.avalith.challenge.models.AplicationUser;
import com.avalith.challenge.services.AplicationUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class JwtUserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private AplicationUserService aplicationUserService;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        AplicationUser aplicationUser = aplicationUserService.findByUsername(username);
        if (aplicationUser != null) {
            return JwtUserFactory.create(aplicationUser);
        }
        throw new UsernameNotFoundException("User not found..");
    }

}
