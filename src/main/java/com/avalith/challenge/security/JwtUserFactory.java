package com.avalith.challenge.security;

import com.avalith.challenge.models.AplicationUser;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import java.util.ArrayList;
import java.util.List;

public class JwtUserFactory {

    private JwtUserFactory() {
    }

    public static JwtUser create(AplicationUser aplicationUser) {
        return new JwtUser(aplicationUser.getId(), aplicationUser.getUsername(), aplicationUser.getPassword(),
                mapToGrantedAuthorities(aplicationUser.getRole()));
    }

    private static List<GrantedAuthority> mapToGrantedAuthorities(String rol) {
        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority(rol));
        return authorities;
    }

}
