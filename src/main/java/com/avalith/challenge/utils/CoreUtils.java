package com.avalith.challenge.utils;


import com.avalith.challenge.security.JwtTokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

@Component
public class CoreUtils {

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    public String getUserFromRequest(HttpServletRequest request){
        Map<String, String> map = new HashMap<String, String>();
        Enumeration headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String key = (String) headerNames.nextElement();
            String value = request.getHeader(key);
            map.put(key, value);
        }
        String token = map.get("authorization");
        String user = jwtTokenUtil.getUsernameFromToken(token.split(" ")[1]);
        return user;
    }
}
