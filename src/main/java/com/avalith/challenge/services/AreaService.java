package com.avalith.challenge.services;

import com.avalith.challenge.models.Area;

import java.util.List;
import java.util.Optional;

public interface AreaService {

    List<Area> findAll();

    Optional findById(long id);



}
