package com.avalith.challenge.services.impl;

import com.avalith.challenge.models.AplicationUser;
import com.avalith.challenge.repositories.AplicationUserRepository;
import com.avalith.challenge.services.AplicationUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AplicationUserServiceImpl implements AplicationUserService {

    @Autowired
    private AplicationUserRepository aplicationUserRepository;

    @Override
    public AplicationUser findByUsername(String username) {
        return aplicationUserRepository.findByUsername(username);
    }
}
