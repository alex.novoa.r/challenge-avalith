package com.avalith.challenge.services.impl;

import com.avalith.challenge.models.Employee;
import com.avalith.challenge.repositories.EmployeeRepository;
import com.avalith.challenge.services.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    public List<Employee> getAllExcept(String username){
        return employeeRepository.getAll(username);
    }

    @Override
    public int getCountAll() {
        return employeeRepository.getCountAll();
    }
}
