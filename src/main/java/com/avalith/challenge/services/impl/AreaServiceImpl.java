package com.avalith.challenge.services.impl;

import com.avalith.challenge.models.Area;
import com.avalith.challenge.repositories.AreaRepository;
import com.avalith.challenge.services.AreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AreaServiceImpl implements AreaService {

    @Autowired
    private AreaRepository areaRepository;

    @Override
    public List<Area> findAll() {
        return areaRepository.findAll();
    }

    @Override
    public Optional findById(long id) {
        return areaRepository.findById(id);
    }


}
