package com.avalith.challenge.services.impl;

import com.avalith.challenge.dtos.EmployeeVoteStatistic;
import com.avalith.challenge.models.AplicationUser;
import com.avalith.challenge.models.Area;
import com.avalith.challenge.models.VoteEmployee;
import com.avalith.challenge.repositories.VoteEmployeeRepository;
import com.avalith.challenge.services.VoteEmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class VoteEmployeeServiceImpl implements VoteEmployeeService {

    @Autowired
    private VoteEmployeeRepository voteEmployeeRepository;

    @Override
    public VoteEmployee save(VoteEmployee voteEmployee) {
        return voteEmployeeRepository.save(voteEmployee);
    }

    @Override
    public List<VoteEmployee> findByAplicationUserAndMonth(AplicationUser aplicationUser, int month) {
        return voteEmployeeRepository.findByAplicationUserAndMonth(aplicationUser, month);
    }

    @Override
    public List<EmployeeVoteStatistic> getCountEmployee(int month) {
        return voteEmployeeRepository.getCountEmployee(month);
    }


    @Override
    public List<EmployeeVoteStatistic> findByArea(Area area){
        return voteEmployeeRepository.findByArea(area);
    }

    @Override
    public List<VoteEmployee> findByAplicationUserAndAreaAndMonth(AplicationUser aplicationUser, Area area, int month) {
        return voteEmployeeRepository.findByAplicationUserAndAreaAndMonth(aplicationUser, area, month);
    }
}
