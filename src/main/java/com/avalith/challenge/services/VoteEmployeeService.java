package com.avalith.challenge.services;

import com.avalith.challenge.dtos.EmployeeVoteStatistic;
import com.avalith.challenge.models.AplicationUser;
import com.avalith.challenge.models.Area;
import com.avalith.challenge.models.VoteEmployee;
import java.util.List;

public interface VoteEmployeeService {

    VoteEmployee save(VoteEmployee voteEmployee);

    List<VoteEmployee> findByAplicationUserAndMonth(AplicationUser aplicationUser, int month);

    List<EmployeeVoteStatistic> getCountEmployee(int month);

    List<EmployeeVoteStatistic> findByArea(Area area);

    List<VoteEmployee> findByAplicationUserAndAreaAndMonth(AplicationUser aplicationUser, Area area, int month);
}
