package com.avalith.challenge.services;

import com.avalith.challenge.models.Employee;

import java.util.List;

public interface EmployeeService {

    List<Employee> getAllExcept(String username);

    int getCountAll();
}
