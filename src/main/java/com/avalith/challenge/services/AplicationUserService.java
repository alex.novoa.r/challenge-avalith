package com.avalith.challenge.services;

import com.avalith.challenge.models.AplicationUser;

public interface AplicationUserService {

    AplicationUser findByUsername(String unsername);
}
