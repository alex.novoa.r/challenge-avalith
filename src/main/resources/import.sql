insert into area(description) values ('Team player');
insert into area(description) values ('Technical referent');
insert into area(description) values ('Key player');
insert into area(description) values ('Client satisfaction');
insert into area(description) values ('Motivation');
insert into area(description) values ('Fun');

insert into aplication_user(username, role) values ('alex','ROLE_ADMIN');
insert into aplication_user(username, role) values ('lore','ROLE_USER');
insert into aplication_user(username, role) values ('liss','ROLE_USER');


insert into employee(name, lastname, aplication_user) values ('Alex','Novoa',1);
insert into employee(name, lastname, aplication_user) values ('Lorena','Guerra',2);
insert into employee(name, lastname, aplication_user) values ('Liss','Reyes',3);

